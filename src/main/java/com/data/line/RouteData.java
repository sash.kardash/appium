package com.data.line;

/**
 * Created by dsherstobitov on 10.09.2015.
 */
public class RouteData {

    private String username;
    private String password;


    public RouteData() throws Exception {
//        DataProvider.LINE = new Line();
//        DataProvider.LINE.setIsNLP(false);
    }

    public RouteData(String route) throws Exception {

        switch (route.toLowerCase()) {
            case "kaiseromronclinician":
                username = "kaiseromronclinician";
                password = "Kaiser.2016!";
                break;
            case "testclinician":
                username = "testclinician";
                password = "testadmin";
                break;
            default:
                throw new Exception(route + " route is unsupported yet");
        }

    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }
}
