package com.utils;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * @author Alex Kardash QA at intetics.com - email:sash.kardash@gmail.com
 * 
 */
public class Emailer {

	Properties properties = new Properties();

	String host = LoadProperties.loadProperty("host");
	String port = LoadProperties.loadProperty("port");

	public Emailer() {

		properties.put("mail.smtp.host", "intetics.com");
		properties.put("mail.smtp.port", port);
	}

	public void send(String subj, String text) {
		try {
			Message message = createMessage(subj);
			message.setText(text);
			Transport.send(message);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	public void send(String subj, String text, String fileName) {

		try {

			Message message = createMessage(subj);
			MimeBodyPart messageBodyPart = new MimeBodyPart();

			messageBodyPart
					.setText(text
							+ "\n\n\n"
							+ "How to see images from report correctly:\n"
							+ "(1) Run  regedit.exe\n"
							+ "(2) Navigate to HKEY_CLASSES_ROOT -> jpegfile -> CLSID\n"
							+ "(3) Right-click on “Default” (the Value Name) and select “Modify\n"
							+ "(4) Change “Value data” from {25336920-03F9-11cf-8FD0-00AA00686F13} to {FFE2A43C-56B9-4bf5-9A79-CC6D4285608A}");

			MimeBodyPart attachmentPart = new MimeBodyPart();

			FileDataSource fileDataSource = new FileDataSource(fileName) {
				@Override
				public String getContentType() {
					return "application/octet-stream";
				}
			};

			attachmentPart.setDataHandler(new DataHandler(fileDataSource));
			attachmentPart.setFileName(fileDataSource.getName());

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			multipart.addBodyPart(attachmentPart);

			message.setContent(multipart);

			Transport.send(message);

		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	public Message createMessage(String subj)
			throws UnsupportedEncodingException, MessagingException {
		Session session = Session.getDefaultInstance(properties);
		MimeMessage message = new MimeMessage(session);

		try {
			message.setFrom(new InternetAddress(LoadProperties
					.loadProperty("from"), "Web Driver executer"));
			message.setRecipients(Message.RecipientType.TO, getRecipients());
			message.setSubject(subj);

		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return message;
	}

	private InternetAddress[] getRecipients() throws AddressException {
		String[] tos = LoadProperties.loadProperty("to").split(";");
		InternetAddress[] listInetAddress = new InternetAddress[tos.length];
		try {
			for (int i = 0; i < tos.length; i++) {
				listInetAddress[i] = new InternetAddress(tos[i]);
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return listInetAddress;
	}
}
