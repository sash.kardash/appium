package com.utils;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * @author Alex Kardash QA at intetics.com - email:sash.kardash@gmail.com
 */
public class DAO {

    MySqlConnector connector;

    public DAO() throws Exception {
        connector = new MySqlConnector();
    }

    public DAO(String remote_host) throws Exception {
        connector = new MySqlConnector(remote_host);
    }

    public void execute(String querry) throws ClassNotFoundException, SQLException {
        connector.execUpdate(querry);
    }

    public Map<String, String> get_results_from(String querry, String col) throws Exception {
        return connector.readResCol(querry, col);
    }

    public List<String> get_all_results_from(String querry, String col) throws Exception {
        return connector.readAllResCol(querry, col);
    }

    public void change_db(String db) throws SQLException {
        connector.change_db(db);
    }
}
