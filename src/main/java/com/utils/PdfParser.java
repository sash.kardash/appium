package com.utils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.apache.pdfbox.util.PDFTextStripper;

public class PdfParser {

	String pdfPath;

	public PdfParser(String pdfPath) {
		this.pdfPath = pdfPath;
	}

	@SuppressWarnings("deprecation")
	public PDXObjectImage extractImage() {

		PDDocument document = null;
		Map<String, PDXObjectImage> images = null;

		try {
			document = PDDocument.load(pdfPath);
		} catch (IOException e) {
			e.printStackTrace();
		}

		PDPage page = (PDPage) document.getDocumentCatalog().getAllPages().get(0);
		PDResources resources = page.getResources();

		try {
			images = resources.getImages();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Iterator<String> imageIter = images.keySet().iterator();
		String key = imageIter.next();
		PDXObjectImage image = images.get(key);

		return image;
	}

	/**
	 * Add an image to an existing PDF document.
	 * 
	 * @param inputFile
	 *            The input PDF to add the image to.
	 * @param image
	 *            The filename of the image to put in the PDF.
	 * @param outputFile
	 *            The file to write to the pdf to.
	 * @throws IOException
	 *             If there is an error writing the data.
	 * @throws COSVisitorException
	 *             If there is an error writing the PDF.
	 */

	public void addImage(String imagePath, int pageNumber) throws IOException, COSVisitorException {

		PDDocument doc = null;
		PDXObjectImage ximage = null;
		BufferedImage stampImage = null;

		doc = PDDocument.load(pdfPath);

		PDPage page = (PDPage) doc.getDocumentCatalog().getAllPages().get(pageNumber);

		stampImage = ImageIO.read(new File(imagePath));
		ximage = new PDJpeg(doc, stampImage);
		PDPageContentStream contentStream = new PDPageContentStream(doc, page, true, true);
		contentStream.drawImage(ximage, 250, 20);
		contentStream.close();
		doc.save(pdfPath);
	}

	public String getText() {

		PDFParser parser = null;
		COSDocument cosDoc = null;
		PDFTextStripper pdfStripper = null;
		String parsedText = null;
		PDDocument pdDoc = null;

		try {
			parser = new PDFParser(new FileInputStream(new File(pdfPath)));
			parser.parse();
			cosDoc = parser.getDocument();
			pdDoc = new PDDocument(cosDoc);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			pdfStripper = new PDFTextStripper();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			parsedText = pdfStripper.getText(pdDoc);
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			cosDoc.close();
			pdDoc.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return parsedText;
	}
}
