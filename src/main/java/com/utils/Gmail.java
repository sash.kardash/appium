package com.utils;

import com.data.DataProvider;

import javax.mail.*;
import javax.mail.internet.MimeBodyPart;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

public class Gmail {

    private static Session session;
    private static Store store;
    private static Folder folder;
    private static Message[] messages;
    private final String EMAIL = DataProvider.GMAIL_EMAIL;
    private final String PASSWORD = DataProvider.GMAIL_PASSWORD;
    private final Properties props;

    public Gmail() {

        props = System.getProperties();
        props.setProperty("mail.store.protocol", "imaps");
        session = Session.getDefaultInstance(props, null);
        try {
            store = session.getStore("imaps");
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
        try {
            store.connect("imap.gmail.com", EMAIL, PASSWORD);
            folder = store.getFolder("Inbox");
            folder.open(Folder.READ_WRITE);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public static Message getGoalMessage(String subj) throws Exception {

        Message msg = null;
        if (subj.equals("any") || subj.isEmpty()) {
            return messages[0];
        }

        for (Message ms : folder.getMessages())
            if (ms.getSubject().contains(subj)) {
                System.out.println(ms.getSubject());
                msg = ms;
                break;
            }

        if (msg == null) {
            throw new Exception("Email with " + subj + " subject can't be found");
        }
        return msg;
    }

    public static Message getFirstMsg() throws MessagingException {

        messages = folder.getMessages();
        return messages[0];
    }

    public static String readEmail(Message message) throws IOException, MessagingException {
        return message.getContent().toString();
    }

    public static String read_email_mime(Object content) throws MessagingException, IOException {

        StringBuilder sb = null;

        if (content instanceof Multipart) {

            Multipart multi = ((Multipart) content);
            int parts = multi.getCount();
            for (int j = 0; j < parts; ++j) {
                MimeBodyPart part = (MimeBodyPart) multi.getBodyPart(j);
                InputStream in = part.getInputStream();
                BufferedReader br = null;
                sb = new StringBuilder();

                String line;

                br = new BufferedReader(new InputStreamReader(in));
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
            }
        }

        return sb.toString();
    }

    public static void saveParts(Object content, String filename) throws IOException, MessagingException {

        OutputStream out = null;
        InputStream in = null;
        if (content instanceof Multipart) {
            Multipart multi = ((Multipart) content);
            int parts = multi.getCount();
            for (int j = 0; j < parts; ++j) {
                MimeBodyPart part = (MimeBodyPart) multi.getBodyPart(j);
                if (part.getContent() instanceof Multipart) {
                    saveParts(part.getContent(), filename);
                } else {
                    String extension = "";
                    if (part.isMimeType("text/html")) {
                        extension = "html";
                    } else {
                        if (part.isMimeType("text/plain")) {
                            extension = "txt";
                        } else {
                            extension = part.getDataHandler().getName();
                        }
                        filename = filename + "." + extension;
                        out = new FileOutputStream(new File(filename));
                        in = part.getInputStream();
                        int k;
                        while ((k = in.read()) != -1) {
                            out.write(k);
                        }
                    }
                }
            }
        }
    }

    public void waitForNewEmail() throws MessagingException, InterruptedException {

        int waitingPeriod = 5;
        int counter = 0;
        while (!isNewMessages()) {
            Thread.sleep(waitingPeriod * 1000);
            if (counter > 10) {
                break;
            }
            counter++;
        }
    }

    public boolean isNewMessages() throws MessagingException {
        return folder.getMessageCount() > 0;
    }

    public void clearEmailBox() {

        try {
            messages = folder.getMessages();
            for (Message msg : messages) {
                msg.setFlag(Flags.Flag.DELETED, true);
            }
            folder.close(true);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public void getAttachment() throws MessagingException, IOException {
        Message msg = getFirstMsg();
        String subject = msg.getSubject();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH_mm_ss");
        String filename = "temp/" + dateFormat.format(calendar.getTime()) + "_" + subject;

        saveParts(msg.getContent(), filename);
    }
}
