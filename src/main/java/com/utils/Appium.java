package com.utils;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.AppiumDriver;

import java.io.IOException;
import java.net.URL;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class Appium {

    String driverName = "appium";
    static String hubUrl;
    static String platformName;
    static String platformVersion;
    static String deviceName;
    static String appUrl;
    static String apppackage;
    static String appActivity;

    public AndroidDriver androidDriver;

    public Appium() throws Exception
    {
        hubUrl = LoadProperties.loadProperty("hub");
        platformName = LoadProperties.loadProperty("platformName");
        platformVersion = LoadProperties.loadProperty("platformVersion");
        deviceName = LoadProperties.loadProperty("deviceName");
        appUrl = LoadProperties.loadProperty("app");
        apppackage = LoadProperties.loadProperty("apppackage");
        appActivity = LoadProperties.loadProperty("appActivity");

        startApplication();
        try{Thread.sleep(5000);}catch(Exception e){}
    }



    public void startApplication() throws Exception{


        System.out.println("application has started...................");


        DesiredCapabilities capabilities= new DesiredCapabilities();
        capabilities.setCapability("browserName", "");
        capabilities.setCapability("deviceName","52037a43ecf3633f");
        capabilities.setCapability("platformVersion","7.0");
        capabilities.setCapability("platformName","Android");
        capabilities.setCapability("app","air.com.sensely.asknhs");
        capabilities.setCapability("apppackage","air.com.sensely.asknhs");
        capabilities.setCapability("appActivity",".AppEntry");
        capabilities.setCapability("noReset","true");
        capabilities.setCapability("newCommandTimeout",3000);
        capabilities.setCapability(MobileCapabilityType.TAKES_SCREENSHOT,true);
        androidDriver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        System.out.println("Appium SetUp for Android is successful and Appium Driver is launched successfully");
        androidDriver.manage().timeouts().implicitlyWait(180, TimeUnit.SECONDS);
        Thread.sleep(10000);
    }




}
