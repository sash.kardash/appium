package com.utils;

import org.im4java.core.*;
import org.im4java.process.ArrayListErrorConsumer;
import org.im4java.process.ArrayListOutputConsumer;
import org.im4java.process.OutputConsumer;
import org.yaml.snakeyaml.Yaml;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author Alex Kardash QA at intetics.com - email:sash.kardash@gmail.com
 */
public class ImageUtil {


    public boolean compare (String actualImage, String expectedImage, boolean shouldCrop) throws IM4JavaException, InterruptedException, IOException {
        boolean imagesEqual;
        ArrayListErrorConsumer arrayListErrorConsumer = new ArrayListErrorConsumer();
        ArrayListOutputConsumer outputConsumer = new ArrayListOutputConsumer();

        IMOperation imOperation = new IMOperation();

        imOperation.metric("ae");
        imOperation.fuzz(10.00);
        String cropValue = "[1200x1920+0+0]";
        long nonWhitePixels = 0;
        if (shouldCrop) {
            Info imageInfo = new Info(actualImage,true);
            int height = imageInfo.getImageHeight();
            int width = imageInfo.getImageWidth();
            cropValue = "["+width+"x"+height/2+"+0+"+height/2+"]";
            nonWhitePixels = different_pixels(actualImage, "screenshots/whiteCropped.png", "screenshots/trash.png");
        } else {
            nonWhitePixels = different_pixels(actualImage, "screenshots/white.png", "screenshots/trash.png");
        }
        imOperation.addImage(cropValue);
        imOperation.addImage(cropValue);
        imOperation.addImage();
        CompareCmd compare = new CompareCmd();
        compare.setErrorConsumer(arrayListErrorConsumer);
        compare.setOutputConsumer(outputConsumer);
        try{
            compare.run(imOperation, actualImage, expectedImage, "screenshots/login2.png");
            imagesEqual = true;
            float x = Integer.parseInt(arrayListErrorConsumer.getOutput().get(0))/nonWhitePixels;
            System.out.println("errorConsumer: "+x+"%");
            System.out.println("outputConsumer:"+outputConsumer.getOutput());
        } catch (Exception e){
            imagesEqual = false;
            ArrayList<String> stringArrayList = arrayListErrorConsumer.getOutput();
            System.out.println(stringArrayList);
        }
        return imagesEqual;
    }

    public long different_pixels(String actualImage, String expectedImage, String differenceImage) throws InterruptedException, IOException, IM4JavaException {
        ArrayListErrorConsumer arrayListErrorConsumer = new ArrayListErrorConsumer();
        ArrayListOutputConsumer outputConsumer = new ArrayListOutputConsumer();

        IMOperation imOperation = new IMOperation();
        long result;
        imOperation.metric("ae");
        imOperation.addImage();
        imOperation.addImage();
        imOperation.addImage();
        CompareCmd compare = new CompareCmd();
        compare.setErrorConsumer(arrayListErrorConsumer);
        compare.setOutputConsumer(outputConsumer);

        try{
            compare.run(imOperation, actualImage, expectedImage, differenceImage);
            System.out.println("arrayListErrorConsumer.getOutput().get(0): " + arrayListErrorConsumer.getOutput().get(0));
            result = Long.parseLong(arrayListErrorConsumer.getOutput().get(0));
            System.out.println("result:"+result);
        } catch (Exception e){
            result = 0;
            ArrayList<String> stringArrayList = arrayListErrorConsumer.getOutput();
            System.out.println("errors: " + stringArrayList);
        }
        return result;
    }

    public boolean crop(String inputImage, String outputImage, int width, int height, int left, int top) throws IOException {
        try {
            // create command
            ConvertCmd cmd = new ConvertCmd();
            // create the operation, add images and operators/options
            IMOperation op = new IMOperation();
            op.addImage(inputImage);
            op.crop(width, height, left, top);
            op.addImage(outputImage);
            cmd.run(op);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }


    /**
     * @param actualImage
     * @param expectedImage
     * @param diffImage
     * @return
     * @throws IOException
     * @throws InterruptedException
     * @throws IM4JavaException
     */
    public boolean compareImages(String actualImage, String expectedImage, String diffImage)
        throws IOException, InterruptedException, IM4JavaException {
        ArrayListErrorConsumer arrayListErrorConsumer = new ArrayListErrorConsumer();
        ArrayListOutputConsumer outputConsumer = new ArrayListOutputConsumer();

        IMOps cmpOp = new IMOperation();
        cmpOp.metric("ae");
        cmpOp.fuzz(10.00);
        cmpOp.addImage();
        cmpOp.addImage();
        cmpOp.addImage();
        CompareCmd compare = new CompareCmd();
        compare.setErrorConsumer(arrayListErrorConsumer);
        try {
            compare.run(cmpOp, actualImage, expectedImage);
            System.out.println("**Image Is Same**");
            return true;
        } catch (Throwable e) {
            try {
                compare.run(cmpOp, actualImage, expectedImage, diffImage);

            } catch (Throwable e1) {
                System.out.println(
                    "Total Pixel Difference of the Images:::" + arrayListErrorConsumer.getOutput()
                        .get(0));
            }
            System.out.println("Total Pixel Difference of the Images:::" + arrayListErrorConsumer.getOutput().get(0));
            return false;
        }

    }

    /**
     * returns true if the images are similar
     * returns false if the images are not identical
     *
     * @param actualImage
     * @param expectedImage
     * @return
     * @throws IOException
     * @throws InterruptedException
     * @throws IM4JavaException
     */
    public boolean compareImages(String actualImage, String expectedImage)
        throws IOException, InterruptedException, IM4JavaException {
        ArrayListErrorConsumer arrayListErrorConsumer = new ArrayListErrorConsumer();
        ArrayListOutputConsumer outputConsumer = new ArrayListOutputConsumer();

        IMOps cmpOp = new IMOperation();
        cmpOp.metric("ae");
        cmpOp.fuzz(40000.00);
        cmpOp.addImage();
        cmpOp.addImage();
        cmpOp.addImage();
        CompareCmd compare = new CompareCmd();
        compare.setErrorConsumer(arrayListErrorConsumer);
        try {
            compare.run(cmpOp, actualImage, expectedImage);
            return true;
        } catch (Throwable e) {
            return false;
        }
    }

    /**
     * @param actualImage actualImagePath
     * @param value       set the ignore % of image pixels
     * @throws IOException
     */
    public boolean compareImages(String expectedImage, String actualImage, String diffImage,
        int value) throws IOException, IM4JavaException, InterruptedException {
        ArrayListErrorConsumer arrayListErrorConsumer = new ArrayListErrorConsumer();
        ArrayListOutputConsumer outputConsumer = new ArrayListOutputConsumer();

        compareImages(actualImage, expectedImage, diffImage);
        System.out.println("expectedImage = "+expectedImage+" actualImage = "+actualImage+" diffImage = "+diffImage);
        long totalImagePixel = getCompleteImagePixel(actualImage);
        if (arrayListErrorConsumer.getOutput().get(0).contains("+") == true) {
            return false;
        } else {
            long totalPixelDifferne = Integer.parseInt(arrayListErrorConsumer.getOutput().get(0));
            double c = ((double) totalPixelDifferne / totalImagePixel) * 100;
            long finalPercentageDifference = Math.round(c);
            System.out.println("Difference in the images is ::" + finalPercentageDifference + "%");
            try {
                if (finalPercentageDifference <= value) {
                    return true;
                }
            } catch (NumberFormatException e) {
            }
            return false;
        }
    }

    /**
     * @param actualImage
     * @param maskImage
     * @param maskedImage
     * @throws IOException
     * @throws InterruptedException
     * @throws IM4JavaException
     */
    public void maskImage(String actualImage, String maskImage, String maskedImage)
        throws IOException, InterruptedException, IM4JavaException {
        IMOperation op = new IMOperation();
        op.addImage(actualImage);
        op.addImage(maskImage);
        op.alpha("on");
        op.compose("DstOut");
        op.composite();
        op.addImage(maskedImage);
        ConvertCmd convert = new ConvertCmd();
        convert.run(op);
    }

    /**
     * @throws InterruptedException
     * @throws IOException
     * @throws IM4JavaException
     */
    public void mergeImagesHorizontally(String expected, String actual, String diffImage,
        String mergedImage) throws InterruptedException, IOException, IM4JavaException {
        ConvertCmd cmd1 = new ConvertCmd();
        IMOperation op1 = new IMOperation();
        op1.addImage(expected); // source file
        op1.addImage(actual); // destination file file
        op1.addImage(diffImage);
        //op1.resize(1024,576);
        op1.p_append();
        op1.addImage(mergedImage);
        cmd1.run(op1);
    }


    public int getCompleteImagePixel(String actualImage) throws IOException {
        BufferedImage readImage = null;
        readImage = ImageIO.read(new File(actualImage));
        int h = readImage.getHeight();
        int w = readImage.getWidth();
        return h * w;
    }

    public void maskRegions(String imageToMaskRegion, String imageMasked, String screenName)
        throws InterruptedException, IOException, IM4JavaException {
        if (checkIfMaskRegionExists(screenName)) {
            drawRectangleToIgnore(imageToMaskRegion, imageMasked, screenName);
        }
    }

    private void drawRectangleToIgnore(String imageToMaskRegion, String imageMasked,
        String screenName) throws IOException, InterruptedException, IM4JavaException {
        ConvertCmd reg = new ConvertCmd();
        IMOperation rep_op = new IMOperation();
        rep_op.addImage(imageToMaskRegion);
        rep_op.fill("Blue");
        rep_op.draw(fetchValueFromYaml(screenName));
        rep_op.addImage(imageMasked);
        reg.run(rep_op);
    }

    public String fetchValueFromYaml(String screenName) throws FileNotFoundException {
        Set mask_region =
            ((LinkedHashMap) ((LinkedHashMap) getYamlParams().get(System.getenv("MASKIMAGE")))
                .get(screenName)).entrySet();
        String maskingRegions = "";
        for (Object regions : mask_region) {
            maskingRegions =
                maskingRegions + " rectangle " + regions.toString().split("=")[1].toString()
                    .replace("[", "").replace("]", "").trim();
        }
        return maskingRegions;
    }

    public boolean checkIfMaskRegionExists(String screenName) throws FileNotFoundException {
        if ((getYamlParams().get(System.getenv("MASKIMAGE"))) != null) {
            if (((LinkedHashMap) getYamlParams().get(System.getenv("MASKIMAGE"))).get(screenName)
                != null
                || ((LinkedHashMap) getYamlParams().get(System.getenv("MASKIMAGE"))).get(screenName)
                != null) {
                return true;
            }
        }
        return false;
    }

    public Map<String, Object> getYamlParams() throws FileNotFoundException {
        final String fileName = System.getProperty("user.dir") + "/nakal.yaml";
        Yaml yaml = new Yaml();
        InputStream yamlParams = new FileInputStream(new File(fileName));
        Map<String, Object> result = (Map<String, Object>) yaml.load(yamlParams);
        return result;
    }

    public boolean checkIfYamlFileExists() {
        final String fileName = System.getProperty("user.dir") + "/nakal.yaml";
        File file = new File(fileName);
        return file.exists();
    }

}
