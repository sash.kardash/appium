package com.soap;

import com.utils.XmlBuilder;
import org.apache.log4j.Logger;

import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

public class SOAPClient {

    private final Logger logger = Logger.getLogger(SOAPClient.class);
    private final String url;
    private final SOAPConnection connection;
    XmlBuilder reader;

    public SOAPClient(String url) throws UnsupportedOperationException, SOAPException {
        this.url = url;
        connection = get_connection();
    }

    public SOAPMessage call(SOAPMessage request) throws SOAPException {
        logger.info("Call " + url);
        return connection.call(request, url);
    }

    private SOAPConnection get_connection() throws UnsupportedOperationException, SOAPException {

        logger.info("Creating soap connection");
        SOAPConnectionFactory soapConnFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection connection = soapConnFactory.createConnection();
        return connection;
    }

    public void close_connection() throws SOAPException {
        connection.close();
    }
}
