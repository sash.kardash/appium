package com.soap;

import com.utils.FileHelper;
import com.utils.XmlBuilder;
import org.apache.log4j.Logger;
import org.w3c.dom.Element;

import javax.xml.soap.SOAPMessage;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.util.Date;

public class SOAPResponse {

    private final Logger logger = Logger.getLogger(SOAPResponse.class);
    public XmlBuilder reader;
    public SOAPMessage soapResponse;


    public SOAPResponse() {

    }

    public SOAPResponse(SOAPMessage soapRequest, SOAPMessage soapResponse, String serviceType) throws Exception {

        logger.info("Reading response");
        this.soapResponse = soapResponse;
        reader = new XmlBuilder(soapResponse);

        if (serviceType.equals("singularity")) {
            String errorCode = reader.getElementsByTag("status").item(0).getTextContent();
            String errorMessage = reader.getElementsByTag("message").item(0).getTextContent();

            if (errorCode.equals("failure") && !errorMessage.contains("Telephone number not found in the BT")) {
                long time = new Date().getTime();
                throw new Exception(errorMessage + ", time is " + time);
            }
        } else if (serviceType.equals("isj")) {


            if (reader.getElementsByTag("faultcode").item(0) != null) {
                String errorCode = reader.getElementsByTag("faultcode").item(0).getTextContent();
                String errorMessage = reader.getElementsByTag("faultstring").item(0).getTextContent();

                if (!errorCode.isEmpty()) {
                    long time = new Date().getTime();
                    print_soap_message("response", soapResponse, time);
                    throw new Exception(errorMessage + ", time is " + time);
                }
            }
        } else throw new Exception(serviceType + " is unsupported service type");

    }

    public String get_exchange_code() throws Exception {

        logger.info("Getting exchange code");
        Element exchCodeContainer = (Element) reader.getElementsByTag("exchangeInfo").item(0);
        String exchCode = exchCodeContainer.getElementsByTagName("exchangeCode").item(0).getTextContent();
        logger.info("Exchange code recieved: " + exchCode);
        return exchCode;
    }

    public String get_tag_value(String tagName) {
        return ((Element) reader.getElementsByTag(tagName).item(0)).getTextContent();
    }

    public void print_soap_message(String fileName, SOAPMessage soapResponse, long time) throws Exception {

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        Source sourceContent = soapResponse.getSOAPPart().getContent();
        StreamResult result = new StreamResult(new StringWriter());
        transformer.transform(sourceContent, result);
        String xmlString = result.getWriter().toString();
        new FileHelper("logs/" + time + "_" + fileName + ".xml", true).writeTofile(xmlString);
        logger.info("SOAP message: " + xmlString);
    }
}
