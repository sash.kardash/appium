package com.soap;

import org.apache.log4j.Logger;

import javax.xml.namespace.QName;
import javax.xml.soap.*;

/**
 * Created with IntelliJ IDEA.
 * User: dys
 * Date: 23.11.04
 * Time: 11:13
 * To change this template use File | Settings | File Templates.
 */
public class SoapMessages {

    private final Logger logger = Logger.getLogger(SoapMessages.class);
    private String SENDER = "test123456";
    private String CORRELATION_ID = "test123456";

    private SOAPMessage get_kofax_default_message() throws SOAPException {
        MessageFactory factory = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
        SOAPMessage message = factory.createMessage();
        SOAPPart soapPart = message.getSOAPPart();

        SOAPEnvelope envelope = soapPart.getEnvelope();
        SOAPHeader header = envelope.getHeader();
        SOAPBody body = envelope.getBody();

        envelope.removeNamespaceDeclaration(envelope.getPrefix());
        envelope.setPrefix("soapenv");
        header.setPrefix("soapenv");
        body.setPrefix("soapenv");

        envelope.addNamespaceDeclaration("soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
        envelope.addNamespaceDeclaration("ser", "http://services.orange.wlr3.singularity.tshop/");
        return message;
    }

    private SOAPMessage get_isj_default_message() throws SOAPException {
        MessageFactory factory = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
        SOAPMessage message = factory.createMessage();
        SOAPPart soapPart = message.getSOAPPart();

        SOAPEnvelope envelope = soapPart.getEnvelope();
        SOAPHeader header = envelope.getHeader();
        SOAPBody body = envelope.getBody();

        envelope.removeNamespaceDeclaration(envelope.getPrefix());
        envelope.setPrefix("soapenv");
        header.setPrefix("soapenv");
        body.setPrefix("soapenv");

        envelope.addNamespaceDeclaration("soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
        envelope.addNamespaceDeclaration("v1", "http://www.everythingeverywhere.com/external/cinergy/service/HomeBroadband/HomeBroadbandRequest/v1.0");
        envelope.addNamespaceDeclaration("dat", "http://messaging.ei.tmobile.net/datatypes");
        return message;
    }

    public SOAPMessage create_perform_broadband_check_message(String linenumber, String postcode) throws SOAPException {
        logger.info("Creating performBroadbandCheck message with " + linenumber + " / " + postcode);

        SOAPMessage message = get_kofax_default_message();
        SOAPPart soapPart = message.getSOAPPart();
        SOAPEnvelope envelope = soapPart.getEnvelope();
        SOAPBody body = envelope.getBody();

        SOAPBodyElement bodyElement = body.addBodyElement(new QName("", "performBroadbandCheck", "ser"));

        SOAPElement argElement = bodyElement.addChildElement("arg0", "", "");
        argElement.addChildElement("phoneNumber").setTextContent(linenumber);
        argElement.addChildElement("postCode").setTextContent(postcode);
        message.saveChanges();
        return message;
    }

    public SOAPMessage create_address_search_message(String linenumber, String postcode) throws SOAPException {

        logger.info("Creating performAddressSearchQuery message");
        SOAPMessage message = get_kofax_default_message();
        SOAPPart soapPart = message.getSOAPPart();
        SOAPEnvelope envelope = soapPart.getEnvelope();
        SOAPBody body = envelope.getBody();

        SOAPBodyElement bodyElement = body.addBodyElement(new QName("", "performAddressSearchQuery", "ser"));

        SOAPElement argElement = bodyElement.addChildElement("arg0", "", "");
        SOAPElement addressElement = argElement.addChildElement("address", "", "");
        addressElement.addChildElement("postcode").setTextContent(postcode);
        message.saveChanges();
        return message;
    }

    public SOAPMessage create_isj_address_search_message(String postcode, String building, String addressRefference) throws SOAPException {

        logger.info("Creating isj_searchAddressHbb message");
        SOAPMessage message = get_isj_default_message();
        SOAPPart soapPart = message.getSOAPPart();
        SOAPEnvelope envelope = soapPart.getEnvelope();
        SOAPBody body = envelope.getBody();

        SOAPBodyElement bodyElement = body.addBodyElement(new QName("", "searchAddressHbb", "v1"));

        SOAPElement contextElement = bodyElement.addChildElement("eiMessageContext2", "", "");
        contextElement.addChildElement("sender", "dat").addTextNode(SENDER);
        contextElement.addChildElement("correlationId", "dat").addTextNode(CORRELATION_ID);

        SOAPElement messageElement = bodyElement.addChildElement("message", "", "");
        SOAPElement addressElement = messageElement.addChildElement("address", "", "");

        addressElement.addChildElement("postcode").addTextNode(postcode);
        addressElement.addChildElement("thoroughFareNumber").addTextNode(building);

        message.saveChanges();
        return message;
    }

//    public SOAPMessage create_isj_create_sale_session_message(String contractType) throws Exception {
//
//        logger.info("Creating isj_createSalesSessionHbb message");
//
//
//        SOAPMessage message = get_isj_default_message();
//        SOAPPart soapPart = message.getSOAPPart();
//        SOAPEnvelope envelope = soapPart.getEnvelope();
//        SOAPBody body = envelope.getBody();
//
//        SOAPBodyElement bodyElement = body.addBodyElement(new QName("", "createSalesSessionHbb", "v1"));
//
//        SOAPElement contextElement = bodyElement.addChildElement("eiMessageContext2", "", "");
//        contextElement.addChildElement("sender", "dat").addTextNode(SENDER);
//        contextElement.addChildElement("correlationId", "dat").addTextNode(CORRELATION_ID);
//
//        SOAPElement messageElement = bodyElement.addChildElement("message", "", "");
//        SOAPElement addressElement = messageElement.addChildElement("address", "", "");
//
//        addressElement.addChildElement("county").addTextNode(BroadbandDataProvider.LINE_PROPERTIES.getCounty());
//        addressElement.addChildElement("districtCode").addTextNode(BroadbandDataProvider.LINE_PROPERTIES.getDistrictCode());
//        addressElement.addChildElement("locality").addTextNode(BroadbandDataProvider.LINE_PROPERTIES.getLocality());
//        addressElement.addChildElement("postTown").addTextNode(BroadbandDataProvider.LINE_PROPERTIES.getPostTown());
//        addressElement.addChildElement("postcode").addTextNode(BroadbandDataProvider.LINE_PROPERTIES.getPostCode());
//        addressElement.addChildElement("premisesName").addTextNode(BroadbandDataProvider.LINE_PROPERTIES.getPremisesName());
//        addressElement.addChildElement("qualifier").addTextNode(BroadbandDataProvider.LINE_PROPERTIES.getQualifier());
//        addressElement.addChildElement("referenceKey").addTextNode(BroadbandDataProvider.LINE_PROPERTIES.getAddressRefference());
//        addressElement.addChildElement("thoroughFareName").addTextNode(BroadbandDataProvider.LINE_PROPERTIES.getFareName());
//        messageElement.addChildElement("phoneNumber").addTextNode(BroadbandDataProvider.LINE_PROPERTIES.getLineNumber());
//        messageElement.addChildElement("contractType").addTextNode(contractType.toUpperCase());
//        messageElement.addChildElement("mobileBrand").addTextNode("EE");
//
//        message.saveChanges();
//        return message;
//    }

    public SOAPMessage create_isj_add_address_message(String postcode, String building) throws
            Exception {

        logger.info("Creating isj_addAddressHbb message");

        SOAPMessage message = get_isj_default_message();
        SOAPPart soapPart = message.getSOAPPart();
        SOAPEnvelope envelope = soapPart.getEnvelope();
        SOAPBody body = envelope.getBody();

        SOAPBodyElement bodyElement = body.addBodyElement(new QName("", "addAddressHbb", "v1"));

        SOAPElement contextElement = bodyElement.addChildElement("eiMessageContext2", "", "");
        contextElement.addChildElement("sender", "dat").addTextNode(SENDER);
        contextElement.addChildElement("correlationId", "dat").addTextNode(CORRELATION_ID);

        SOAPElement messageElement = bodyElement.addChildElement("message", "", "");
        SOAPElement addressElement = messageElement.addChildElement("address", "", "");

        addressElement.addChildElement("postTown").addTextNode("test");
        addressElement.addChildElement("postcode").addTextNode(postcode);
        addressElement.addChildElement("thoroughFareName").addTextNode("test");
        addressElement.addChildElement("thoroughFareNumber").addTextNode(building);

        message.saveChanges();
        return message;
    }
}
