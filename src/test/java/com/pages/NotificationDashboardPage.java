package com.pages;

import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebDriver;

@DefaultUrl("/patients/notification-dashboard")
public class NotificationDashboardPage extends AbstractPage {

	public NotificationDashboardPage(WebDriver driver) {
		super(driver);
	}

	@Override
	public void init_elements() {

		elements.putAll(get_default_elements());

		elements.put("logout button", "//ul[@class='menu']//a[text()='Logout']");

		elements.put("notification section", "//div[@class='pane notificationPane']");
		elements.put("patient section", "//div[@class='pane patientPane']");


	}
}
