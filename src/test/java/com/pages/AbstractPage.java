package com.pages;

import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebDriver;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Alex Kardash QA at intetics.com - email:sash.kardash@gmail.com
 */
public class AbstractPage extends PageObject {

    public static Map<String, String> elements;
    private static Object currentPage;

    public AbstractPage(WebDriver driver) {
        super(driver, 15000);
    }

    public static Object getCurrentPage() {
        return currentPage;
    }

    public static void setCurrentPage(AbstractPage currentPage) {
        elements = new HashMap<String, String>();
        AbstractPage.currentPage = currentPage;
        currentPage.init_elements();
    }

    public void init_elements() {
    }

    public Map<String, String> get_default_elements() {

        Map<String, String> elements = new HashMap<>();
        return elements;
    }


}
