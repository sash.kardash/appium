package com.pages;

import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebDriver;

@DefaultUrl("/")
public class LoginPage extends AbstractPage {

	public LoginPage(WebDriver driver) {
		super(driver);
	}

	@Override
	public void init_elements() {
		elements.putAll(get_default_elements());

		elements.put("username field", "//input[@id='edit-name']");
		elements.put("password field", "//input[@id='edit-pass']");
		elements.put("login button", "//input[@id='edit-submit']");
	}
}
