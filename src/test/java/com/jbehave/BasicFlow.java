package com.jbehave;

import com.steps.BasicSteps;
import com.utils.Parser;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.junit.Assert;
import org.openqa.selenium.Keys;

/**
 * @author Alex Kardash QA at intetics.com - email:sash.kardash@gmail.com
 */
public class BasicFlow {

    @Steps
    BasicSteps basicSteps;

    @Given("user on the '$page' page")
    public void user_on_specified_page(String page) throws ClassNotFoundException {
        basicSteps.open_page(page);
        basicSteps.clear_emailbox();
    }

    @Given("'$text' info message")
    @When("'$text' info message")
    @Then("'$text' info message")
    public void info_message(String text) {
    }

    @When("click on '$elementName'")
    public void click_on(String elementName) throws Exception {
        basicSteps.click_on(elementName);
        waitabit("1000");
    }

    @When("click '$linktext' link")
    public void click_on_link(String linktext) {
        basicSteps.click_on_link(linktext);
    }

    @When("click '$linktext' link in '$section'")
    public void click_on_link_in_section(String linktext, String section) throws Exception {
        basicSteps.click_on_link_in_section(linktext, section);
    }

    @When("click '$buttonText' button")
    public void click_on_button(String buttonText) {
        basicSteps.click_on_button(buttonText);
    }

    @When("enter '$text' in '$elementName'")
    public void enter_in(String text, String elementName) throws Exception {
        basicSteps.enter_in(text, elementName);
    }

    @When("press '$buttonName' for '$elementName'")
    public void press(Keys buttonName, String elementName) throws Exception {
        basicSteps.press(buttonName, elementName);
    }

    @When("select '$text' from '$elementName'")
    public void select_from_dropdown(String text, String elementName) throws Exception {
        basicSteps.select_from_dropdown(text, elementName);
    }

    @When("select '$value' value from '$elementName'")
    public void select_from_dropdown_byval(String value, String elementName) throws Exception {
        basicSteps.select_from_dropdown_byval(value, elementName);
    }

    @When("wait '$milsecs'")
    public void waitabit(String milsecs) {
        basicSteps.waitABit(Long.parseLong(milsecs));
    }

    @When("open '$urlPrefix' url")
    public void open_url(String urlPrefix) throws Exception {

        String parentUrl = basicSteps.getPages().getDefaultBaseUrl();
        if (urlPrefix.contains("http") || urlPrefix.contains("https") || urlPrefix.contains("www.")) {
            basicSteps.getDriver().navigate().to(urlPrefix);
        } else {
            basicSteps.getDriver().navigate().to(parentUrl + urlPrefix);
        }
        waitabit("2000");
    }

    @When("restart browser")
    public void restart_browser() {
        basicSteps.restart_browser();
    }

    @Then("see '$text' message")
    public void see_message(String text) throws Exception {
        basicSteps.see_message(text);
    }

    @Then("see '$message' in source")
    public void see_in_source(String message) throws Exception {
        basicSteps.see_in_source(message);
    }

    @Then("no '$message' in source")
    public void no_in_source(String message) throws Exception {
        basicSteps.no_in_source(message);
    }

    @Then("see '$sectionText' message in '$section'")
    public void see_message_in_section(String sectionText, String section) throws Exception {
        basicSteps.see_message_in_section(sectionText, section);
    }

    @Then("no '$text' message")
    public void dont_see_message(String message) throws Exception {
        basicSteps.no_message(message);
    }

    @Then("no '$text' message in '$section'")
    public void dont_see_message_in_section(String message, String section) throws Exception {
        basicSteps.no_message_in_section(message, section);
    }

    @Then("see '$maskedText' masked message")
    public void see_masked_message(String message) throws Exception {
        basicSteps.see_masked_message(message);
    }

    @Then("pop up '$popupMessage' appears")
    public void popup_appears(String popupMessage) throws Exception {
        basicSteps.popup_appears(popupMessage);
    }

    /**
     * @param expectedState = "not available", "available", "enabled", "disabled", "checked", "active
     */
    @Then("'$element' is '$expectedState'")
    public void element_is(String elementName, String expectedState) throws Exception {
        basicSteps.element_is(elementName, expectedState);
    }

    @Then("'$dropdownName' has '$optionList' options")
    public void dropdown_has_options(String dropDown, String optionList) throws Exception {
        basicSteps.has_options(dropDown, optionList.split(";"));
    }

    @Then("email '$subj' recieved")
    public void email_recieved(String subj) throws Exception {
        basicSteps.wait_for_email(subj);
    }

    @Then("email contains text '$expetctedTest'")
    public void email_contains_text(String text) throws Exception {
        basicSteps.wait_for_email("confirmation");
        basicSteps.check_email_for_text(text);
    }

    @Then("go back")
    public void go_back() {
        basicSteps.go_back();
    }

    @Then("'$alertText' alert display")
    public void is_alert_display(String alertText) {
        basicSteps.is_alert_display(alertText);
    }

    @Then("user on the '$page'")
    public void user_on_the_page(String pageName) throws Exception {
        basicSteps.set_page(pageName);
        waitabit("1500");
    }

    @Then("'$columnList' columns has '$expectedValueList' value in '$goalTable' table where '$whereClause'")
    public void is_db_has_value(String columnList, String goalTable, String expectedValueList, String whereClause) throws Exception {
        basicSteps.is_db_has_value(columnList, goalTable, expectedValueList, whereClause);
    }

//    @Then("'$columnList' columns has '$expectedValues' value for this order")
//    public void is_order_has_correct_values_in_db(String columnList, String expectedValues) throws Exception {
//
//        String table = "home_broadband_registration";
//        String order_id = DataProvider.ROUTE.getDki().replace("DKI", "");
//        while (order_id.startsWith("7") || order_id.startsWith("0")) {
//            order_id = order_id.substring(1);
//        }
//        is_db_has_value(columnList, table, expectedValues, "id=" + order_id);
//    }

    @Then("'$linkName' link contains '$expectedUrl' url")
    public void link_contains_url(String linkName, String expectedUrl) throws Exception {
        basicSteps.link_contains_url(linkName, expectedUrl);
    }

    @Then("'$linkName' link contains '$expectedUrl' url in '$section'")
    public void link_contains_url_in_section(String linkName, String expectedUrl, String section) throws Exception {
        basicSteps.link_contains_url_in_section(linkName, expectedUrl, section);
    }

    @Then("show text for '$xpath'")
    public void show_element_text(String xpath) throws Exception {
        basicSteps.show_element_text(xpath);
    }

    @Then("show message '$message'")
    public void show_message(String message) throws Exception {
        basicSteps.show_message(message);
    }

    @Then("page url is '$expectedUrl'")
    public void page_url(String expectedUrl) throws Exception {
        String url = basicSteps.getDriver().getCurrentUrl();
        String parentUrl = basicSteps.getPages().getDefaultBaseUrl();
        String resultUrl;
        if (expectedUrl.contains("http") || expectedUrl.contains("https") || expectedUrl.contains("www.")) {
            resultUrl = url;
        } else {
            resultUrl = url.replace(parentUrl,"");
        }
        Assert.assertTrue("Expected" + expectedUrl + " url not equals to real " + resultUrl, expectedUrl.equals(resultUrl));
        waitabit("2000");
    }


    @Then("'$pageName' page content correct")
    public void page_content_correct(String pageName) throws Exception {
        basicSteps.page_content_correct(pageName);
    }

    public String getBuildMeta() throws Exception {

        String version = "";
        String buildDate = "";

        try {
            Document doc = Parser.getContent(Connection.Method.GET, basicSteps.getPages().getDefaultBaseUrl() + "/version.jsp").parse();

            version = doc.select("body tr:contains(Project version)").text().replace("Project version: ", "");
            buildDate = doc.select("body tr:contains(Project build date)").text().replace("Project build date: ", "");
        } catch (Throwable e) {

        }
        return "Build# " + version + " from " + buildDate;
    }


    /////////////////////////// Mobile


}
