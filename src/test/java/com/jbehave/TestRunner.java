package com.jbehave;

import net.serenitybdd.jbehave.SerenityStories;
import org.jbehave.core.annotations.AfterScenario;
import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.BeforeStories;
import org.junit.Test;

/**
 * @author Alex Kardash QA at intetics.com - email:sash.kardash@gmail.com
 */
public class TestRunner extends SerenityStories {

    public TestRunner() throws Exception {
        runSerenity().inASingleSession();
    }

    @Test
    public void run() throws Throwable {
        super.run();
    }

    @BeforeStories
    public void setup() throws Exception {
        set_driver();
//        new StoryProvider().generate_report();
    }

    @BeforeScenario
    public void reset() throws Exception {
    }

    @AfterScenario
    public void clear() throws Exception {
    }

    public void set_driver() {
        String driver = System.getProperty("webdriver.driver");

        if (driver == null) {
            System.setProperty("webdriver.driver", "firefox");
        } else if (driver.equals("iexplorer")) {
            System.setProperty("webdriver.ie.driver", "src/main/resources/drivers/IEDriverServer.exe");
        } else if (driver.equals("chrome")) {
            System.setProperty("webdriver.chrome.driver", "src/main/resources/drivers/chromedriver.exe");
            System.setProperty("webdriver.chrome.logfile", "logs/chromedriver.log");
        } else {
            System.setProperty("webdriver.driver", "firefox");

        }

    }
}
