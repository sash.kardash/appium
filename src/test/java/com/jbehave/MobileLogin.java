package com.jbehave;

import com.data.DataProvider;
import com.data.line.RouteData;
import com.steps.LoginSteps;
import com.steps.MobileLoginSteps;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

public class MobileLogin extends BasicFlow {

    @Steps
    MobileLoginSteps mobileLoginSteps;

    @Given("user on the mobile login page")
    public void open_mobile_login_page() throws Exception {
        mobileLoginSteps.open_mobile_login_page();
    }

    @When("user on the screen")
    public void user_on_the_screen() throws Exception {
        mobileLoginSteps.user_on_the_screen("qwe");
    }

    @When("login on mobile with '$username', '$password'")
    public void login_with_mobile(String username, String password) throws Exception {
        mobileLoginSteps.login_with_mobile(username, password);
    }

    @When ("signup on mobile with '$productcode'")
    public void signup_with_mobile(String productcode) throws Exception {
        mobileLoginSteps.signup_with_mobile(productcode);
    }

    @When ("swipe")
    public void swipe() throws Exception {
        mobileLoginSteps.swipe();
    }

    @When ("type '$text' in '$fieldname'")
    public void type_in(String text, String fieldName) throws Exception {
        mobileLoginSteps.type_in(text,fieldName);
    }

    @When ("tap on '$elementname'")
    public void tap_on(String elementname) throws Exception {
        mobileLoginSteps.tap_on(elementname);
    }

    @When("take screenshot '$name'")
    public void take_screenshot(String name) throws Exception {
        mobileLoginSteps.takeScreenShot(name);
    }




}

