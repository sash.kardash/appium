package com.jbehave;

import com.data.DataProvider;
import com.data.line.RouteData;
import com.steps.LoginSteps;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

public class Login extends BasicFlow {

    @Steps
    LoginSteps loginSteps;

    @Given("user on the web login page")
    public void open_login_page() throws Exception {
        loginSteps.open_login_page();
        user_on_the_page("LoginPage");

    }

    @When("Login with '$username', '$password'")
    public void login_with(String username, String password) throws Exception {
        enter_in(username, "username field");
        enter_in(password, "password field");
        click_on("login button");
        user_on_the_page("NotificationDashboardPage");
    }


    /**
     * @param route - should be valid route from RouteData, like nlp, elp, ect
     * @throws Exception - in case if route nate wasn't recognized
     */
    @When("login as '$route'")
    public void login_as(String route) throws Exception {
        DataProvider.ROUTE = new RouteData(route);
        login_with(DataProvider.ROUTE.getUsername(), DataProvider.ROUTE.getPassword());
    }

}

