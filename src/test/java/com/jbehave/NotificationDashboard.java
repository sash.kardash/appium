package com.jbehave;

import com.data.DataProvider;
import com.data.line.RouteData;
import com.steps.LoginSteps;
import com.steps.NotificationDashboardSteps;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.When;

public class NotificationDashboard extends BasicFlow {

    @Steps
    NotificationDashboardSteps notificationDashboardSteps;

    @When("click '$elementName' in '$section'")
    public void click_on_element_in_section(String elementName, String section) throws Exception {
        notificationDashboardSteps.click_on_element_in_section(elementName, section);
    }

    @When ("logout")
    public void logout_() throws Exception {
        click_on("logout button");
        user_on_the_page("LoginPage");
    }
}

