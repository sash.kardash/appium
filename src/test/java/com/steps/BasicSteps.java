package com.steps;

import com.data.DataProvider;
import com.pages.AbstractPage;
import com.utils.*;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.mail.MessagingException;
import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Alex Kardash QA at intetics.com - email:sash.kardash@gmail.com
 */
@SuppressWarnings("serial")
public class BasicSteps extends ScenarioSteps {

    private static Logger logger = Logger.getLogger(BasicSteps.class);
    AbstractPage currentPage;
    String PAGE_PATH = "com.pages.";
    String color = "2px groove red";

    public BasicSteps(Pages pages) {
        super(pages);
        getPages().setDefaultBaseUrl(LoadProperties.loadProperty("url"));
        currentPage = (AbstractPage) AbstractPage.getCurrentPage();
    }

    @Step
    public void open_page(String page) throws ClassNotFoundException {

        @SuppressWarnings("unchecked") Class<PageObject> pageObjectClass = (Class<PageObject>) Class.forName(PAGE_PATH + page);
        getPages().get(pageObjectClass).open();
        AbstractPage.setCurrentPage((AbstractPage) getPages().get(pageObjectClass));
    }

    public void wait_for_element_to_hide(String elementName) throws Exception {
        WebElement element = get_element(elementName);
        highlight_element("//img[contains(@src,'waiting.gif')]");
        currentPage.$(element).withTimeoutOf(10, TimeUnit.SECONDS).waitUntilVisible();
    }

    @Step
    public void click_on(String elementName) throws Exception {
        WebElement element = get_element(elementName);
        String xpath = AbstractPage.elements.get(elementName);
        highlight_element(xpath);
        currentPage.$(element).withTimeoutOf(5, TimeUnit.SECONDS).waitUntilClickable().click();
    }

    @Step
    public void click_on_link(String linktext) {
        String xpath = "//a[contains(text(), \" + linktext + \")]";
        WebElement element = currentPage.findBy(xpath);
        highlight_element(xpath);
        element.click();
    }

    @Step
    public void click_on_link_in_section(String linktext, String section) throws Exception {
        String xpath = ".//a[contains(text(), \"" + linktext + "\")]";
        WebElement element = currentPage.$(get_element(section)).then().findBy(xpath);
        highlight_element(element.getAttribute("xpath"));
        element.click();
    }

    @Step
    public void click_on_button(String buttonText) {
        String xpath = "(//*[@style = 'display: block;']//button[contains(text(), '" + buttonText + "')])|(//button[contains(text(), '" + buttonText + "')])";
        WebElement element = currentPage.findBy(xpath);
        highlight_element(xpath);
        currentPage.$(element).withTimeoutOf(3, TimeUnit.SECONDS).waitUntilVisible().click();
    }

    @Step
    public void enter_in(String text, String elementName) throws Exception {

        WebElement element = get_element(elementName);
        String xpath = AbstractPage.elements.get(elementName);
        highlight_element(xpath);

        if (text != null && !text.isEmpty()) {
            currentPage.typeInto(element, text);
        } else element.clear();
    }

    @Step
    public void element_is(String elementName, String expectedState) throws Exception {

        String[] states = {"not available", "available", "enabled", "disabled", "checked", "active"};
        WebElement element;
        if (expectedState.equals(states[0])) {
            boolean passed = false;
            try {
                currentPage.findBy(currentPage.elements.get(elementName)).isPresent();
            } catch (Throwable e) {
                passed = true;
            }
            Assert.assertTrue(elementName + " is available!", passed);
        } else if (expectedState.equals(states[1])) (currentPage.$(get_element(elementName))).waitUntilVisible();
        else if (expectedState.equals(states[2])) {
            if (!(element = get_element(elementName)).getTagName().equals("a"))
                currentPage.$(get_element(elementName)).shouldBeEnabled();
            else Assert.assertTrue(element.getAttribute("disabled") == null);
        } else if (expectedState.equals(states[3])) {
            if (!(element = get_element(elementName)).getTagName().equals("a")) {
                Assert.assertTrue(elementName + " is enabled, but shouldn't", !element.isEnabled() || element.getAttribute("class").contains("disable") || element.getAttribute("class").contains("inactive"));
            } else Assert.assertTrue(element.getAttribute("disabled").contains("true"));
        } else if (expectedState.equals(states[4]))
            Assert.assertTrue("" + elementName + " should be checked!", (get_element(elementName)).getAttribute("checked") != null);
        else if (expectedState.equals(states[5]))
            Assert.assertTrue("" + elementName + " should be active!", (get_element(elementName)).getAttribute("class").equals("active"));

        else
            throw new Exception(expectedState + " state is not supported yet. Available states are: " + Arrays.toString(states));
    }

    @Step
    public void select_from_dropdown(String itemName, String elementName) throws Exception {

        WebElementFacade dropdown = currentPage.$(get_element(elementName));
        String xpath = AbstractPage.elements.get(elementName);
        highlight_element(xpath);

        Integer optionIndex = null;
        if (!itemName.equals("any")) {

            List<String> dropdownOptions = dropdown.getSelectOptions();

            for (String optionName : dropdownOptions) {
                if (optionName.replace(" ", "").replace("\n", "").startsWith(itemName.replace(" ", "").replace("\n", ""))) {
                    optionIndex = dropdownOptions.lastIndexOf(optionName);
                    break;
                }
            }
            if (optionIndex == null) {
                throw new Exception(itemName + " option is not found in " + elementName);
            }
            dropdown.selectByIndex(optionIndex);

        } else {
            dropdown.selectByIndex(2);
        }

//        show_message(dropdown.getSelectedValue() + " was selected");
    }

    @Step
    public void select_from_dropdown_byval(String itemValue, String elementName) throws Exception {
        WebElementFacade dropdown = currentPage.$(get_element(elementName));
        String xpath = AbstractPage.elements.get(elementName);
        highlight_element(xpath);
        dropdown.selectByValue(itemValue);
//        show_message(dropdown.findBy("/preceding::span").getText());
    }

    @Step
    public void press(Keys buttonName, String elementName) throws Exception {
        get_element(elementName).sendKeys(buttonName);
    }

    @Step
    public void press_key(int keyArg) throws AWTException {
        Robot robot = new Robot();
        robot.keyPress(keyArg);
        robot.keyRelease(keyArg);
    }

    @Step
    public void press_double_key(int keyArg1, int keyArg2) throws AWTException {
        Robot robot = new Robot();
        robot.keyPress(keyArg1);
        robot.keyPress(keyArg2);
        robot.keyRelease(keyArg1);
        robot.keyRelease(keyArg2);
    }

    @Step
    public void mouse_click(int keyArg1) throws AWTException {
        Robot robot = new Robot();
        robot.mousePress(keyArg1);
        robot.mouseRelease(keyArg1);
    }

    @Step
    public void move_mouse_to(int x, int y) throws AWTException {
        Robot robot = new Robot();
        robot.mouseMove(x, y);
    }

    @Step
    public void mouse_scroll(int times) throws AWTException {
        Robot robot = new Robot();
        robot.mouseWheel(times);
    }

    @Step
    public void set_clipboard_data(String string) {
        StringSelection stringSelection = new StringSelection(string);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
    }

    @Step
    public void has_options(String dropDown, String[] optionList) throws Exception {

        String error = "";
        List<String> options = currentPage.$(get_element(dropDown)).getSelectOptions();
        for (String option : optionList) {
            if (!options.contains(option.trim()))
                error += option + " wasn't found in the " + dropDown + Arrays.asList(options) + ";";
        }
        if (!error.isEmpty()) throw new Exception(error);
    }

    @Step
    public void see_in_source(String message) throws Exception {
        try {
            String pageText = getDriver().getPageSource();
//            System.out.println("@@@@@@@@@@@@@@6"+pageText);
            Assert.assertTrue(">" + message + "< text can't be found", pageText.replace(" ", "").replace(" ", "").replace("’", "'").replaceAll("\\n", "").replaceAll("\\r", "").replace("\u00a0", "").contains(message.replace(" ", "").replace(" ", "").replace("’", "'")));
        } catch (Throwable e) {
            currentPage.waitForTextToAppear(message);
        }
    }

    @Step
    public void see_message(String text) throws Exception {
        try {

            WebElement body = currentPage.$("//html[@class='no-js js']");
            String pageText = body.getText();
            Assert.assertTrue(">" + text + "< text can't be found in " + pageText, pageText.replace(" ", "").replace(" ", "").replace("’", "'").replace("‘", "'").replaceAll("\\n", "").replaceAll("\\r", "").replace("\u00a0", "").contains(text.replace(" ", "").replace(" ", "").replace("’", "'").replace("‘", "'")));
        } catch (Throwable e) {
            currentPage.waitForTextToAppear(text);
        }
    }

    @Step
    public void see_message_in_section(String text, String section) throws Exception {
        try {
            String sectionText = currentPage.$(get_element(section)).getText();
            System.out.println("@@@@@@@@@@@@@@6" + sectionText.toLowerCase()
                    .replace(" ", "")
                    .replace(" ", "")
                    .replace("’", "'")
                    .replaceAll("\\n", "")
                    .replaceAll("\\r", "")
                    .replace("\u00a0", ""));

            System.out.println("@@@@@@@@@@@@@@7" + text.toLowerCase()
                    .replace(" ", "")
                    .replace(" ", "")
                    .replace("’", "'")
                    .replace("вј", "?"));
            show_message(sectionText.toLowerCase()
                    .replace(" ", "")
                    .replace(" ", "")
                    .replace("’", "'")
                    .replaceAll("\\n", "")
                    .replaceAll("\\r", "")
                    .replace("\u00a0", ""));
            show_message(text.toLowerCase()
                    .replace(" ", "")
                    .replace(" ", "")
                    .replace("’", "'"));
            Assert.assertTrue(">" + text + "< text can't be found", sectionText.toLowerCase()
                    .replace(" ", "")
                    .replace(" ", "")
                    .replace("’", "'")
                    .replaceAll("\\n", "")
                    .replaceAll("\\r", "")
                    .replace("\u00a0", "")
                    .contains(text.toLowerCase()
                            .replace(" ", "")
                            .replace(" ", "")
                            .replace("’", "'")
                            .replace("вј", "")));
        } catch (Throwable e) {
            currentPage.waitForTextToAppear(text);
        }

    }

    @Step
    public void no_in_source(String message) {
        String pageText = getDriver().getPageSource();
        Assert.assertFalse(">" + message + "< text is be found", pageText.replace(" ", "").replace(" ", "").replace("’", "'").replaceAll("\\n", "").replaceAll("\\r", "").replace("\u00a0", "").contains(message.replace(" ", "").replace(" ", "").replace("’", "'")));
    }

    @Step
    public void no_message(String message) throws Exception {

        WebElement body = currentPage.$("//html");

        String pageText = body.getText();
        Assert.assertFalse(">" + message + "< text is found", pageText.replace(" ", "").replace(" ", "").replaceAll("\\n", "").replaceAll("\\r", "").replace("\u00a0", "").contains(message.replace(" ", "").replace(" ", "")));
    }

    @Step
    public void no_message_in_section(String message, String section) throws Exception {
        String sectionText = currentPage.$(get_element(section)).getText();
        Assert.assertFalse(">" + message + "< text is found", sectionText.replace(" ", "").replace(" ", "").replaceAll("\\n", "").replaceAll("\\r", "").replace("\u00a0", "").contains(message.replace(" ", "").replace(" ", "")));
    }

    @Step
    public void see_masked_message(String message) {

        String pageText = Jsoup.parse(getDriver().getPageSource()).text();
        Pattern pattern = Pattern.compile(message);
        Matcher matcher = pattern.matcher(pageText);

        Assert.assertTrue("Masked >" + message + "< wasn't found on the page", matcher.find());
    }

    @Step
    public void clear_emailbox() {
        new Gmail().clearEmailBox();
    }

    @Step
    public void wait_for_email(String subj) throws Exception {
        new Gmail().waitForNewEmail();
        DataProvider.COMM_EMAIL = Gmail.getGoalMessage(subj);
    }

    @Step
    public void check_email_for_text(String text) throws InterruptedException, IOException, MessagingException {
        String email_text = Gmail.read_email_mime(DataProvider.COMM_EMAIL.getContent());
        String emailText = Parser.getContent(email_text).text();
        Assert.assertTrue(text + " not found in " + emailText,
                emailText.replace(" ", "")
                        .replace("‘", "'")
                        .replace("’", "'")
                        .replaceAll("\\n", "")
                        .replaceAll("\\r", "")
                        .replace("\u00a0", "")
                        .contains(text.replace(" ", "")
                                .replace("‘", "'")
                                .replace("’", "'")
                                .replaceAll("\\n", "")
                                .replaceAll("\\r", "")
                                .replace("\u00a0", "")));
    }

    @Step
    public void go_back() {
        getDriver().navigate().back();
    }

    @Step
    public void is_db_has_value(String columnList, String goalTable, String expectedValueList, String whereClause) throws Exception {

        Map<String, String> results = new DAO().get_results_from("SELECT " + columnList + " FROM " + goalTable + " WHERE " + whereClause, columnList);

        Iterator it = results.entrySet().iterator();
        int i = 0;
        while (it.hasNext()) {
            Map.Entry<?, ?> pairs = (Map.Entry<?, ?>) it.next();
            String expectedValue = "";
            try {
                expectedValue = expectedValueList.split(",")[i].replace("''", "");
            } catch (ArrayIndexOutOfBoundsException e) {

            }
            show_message("Is " + pairs.getKey() + "='" + expectedValue + "' for the " + whereClause);
            show_message(pairs.getKey() + " col value is " + pairs.getValue() + ", should be " + expectedValue);
            Assert.assertTrue(pairs.getKey() + " col value is " + pairs.getValue() + ", should be " + expectedValue, String.valueOf(pairs.getValue()).equals(expectedValue));
            i++;
        }
    }

    @Step
    public void get_all_values_from_db(String columnList, String goalTable, String whereClause) throws Exception {

        List<String> results = new DAO().get_all_results_from("SELECT " + columnList + " FROM " + goalTable + " WHERE " + whereClause, columnList);

        System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@" + results);
//
//        Iterator it = results.entrySet().iterator();
//        int i = 0;
//        while (it.hasNext()) {
//            Map.Entry<?, ?> pairs = (Map.Entry<?, ?>) it.next();
//            String expectedValue = "";
//            try {
//                System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@" + results);
//                expectedValue = expectedValueList.split(",")[i];
//            } catch (ArrayIndexOutOfBoundsException e) {
//
//            }
//            show_message("Is " + pairs.getKey() + "='" + expectedValue + "' for the " + whereClause);
//            Assert.assertTrue(pairs.getKey() + " col value is " + pairs.getValue() + ", should be " + expectedValue, String.valueOf(pairs.getValue()).equals(expectedValue));
//            i++;
//        }
    }

    @Step
    public void show_message(Object message) {
        logger.info(message);
    }

    @Step
    public void popup_appears(String popupMessage) throws Exception {
        see_message(popupMessage);
        currentPage.findBy("//*[contains(@style, 'block')]").click();
    }

    @Step
    public void is_alert_display(String alertText) {
        Assert.assertTrue("Expected alert not found, there is " + currentPage.getAlert().getText() + " alert", currentPage.getAlert().getText().replace("\n", "").contains(alertText));
        currentPage.getAlert().accept();
    }

    public void set_page(String pageName) throws Exception {

        @SuppressWarnings("unchecked") final Class<PageObject> pageObjectClass = (Class<PageObject>) Class.forName(PAGE_PATH + pageName);
        AbstractPage.setCurrentPage((AbstractPage) getPages().get(pageObjectClass));
        try {
            ExpectedCondition<Boolean> e = new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver driver) {
                    return (getPages().isCurrentPageAt(pageObjectClass));
                }
            };
            new WebDriverWait(getDriver(), 10).until(e);
        } catch (Exception e) {
            throw new IllegalStateException("Current " + pageName + " is not according to the expected url, current url is " + getDriver().getCurrentUrl());
        }
    }

    @Step
    public void validate(String errMsg) throws Exception {
        if (!errMsg.isEmpty()) throw new Exception(errMsg);
    }

    @Step
    public void check_is(String message, boolean equals) {
        Assert.assertTrue(message, equals);
    }


    public WebElement get_element(String elementName) throws Exception {

        String locator = AbstractPage.elements.get(elementName);
        if (locator == null) {
            throw new Exception("Element specified by '" + elementName + "' is not added in the " + AbstractPage.getCurrentPage().getClass().getName() + " class");
        }

        WebElement element;
        try {
            element = currentPage.findBy(locator);
            return element;
        } catch (Throwable e) {
            throw new Exception("Can't find " + elementName + " on this page, due element unavailability or " + locator + " locator invalid");
        }
    }

    @Step
    public void restart_browser() {
        try {
            getDriver().quit();
        } catch (Throwable e) {

        }
        getDriver().get(DataProvider.ENVIROMENT_URL);
    }

    void highlight_element(String xpath) {
        try {
            currentPage.evaluateJavascript("(document.evaluate(\"" + xpath + "\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue)" +
                    ".style.border=\"" + color + "\";");
        } catch (Exception e) {
        }
    }

    public List<String> get_digits_array_from(String text) {
        Pattern p = Pattern.compile("([0-9]+\\.?[0-9]{0,2})");
        Matcher m = p.matcher(text);

        int count = 0;
        List<String> digitsInText = new ArrayList<>();
        while (m.find()) {
            digitsInText.add(m.group());
        }
        return digitsInText;
    }

    public void link_contains_url(String linkName, String expectedUrl) throws Exception {
        String xpath = "(//*[@style = 'display: block;']//a[contains(text(), '" + linkName + "')])|(//a[contains(text(), '" + linkName + "')])";
        WebElement element = currentPage.$(xpath);
        highlight_element(xpath);
        if (expectedUrl.startsWith("#")) {
            expectedUrl = getDriver().getCurrentUrl() + expectedUrl;
        } else if (expectedUrl.startsWith("/")) {
            expectedUrl = DataProvider.ENVIROMENT_URL + expectedUrl;
        }
        Assert.assertTrue(expectedUrl + " url not found in " + linkName + "link", element.getAttribute("href").equals(expectedUrl));
    }

    public void link_contains_url_in_section(String linkName, String expectedUrl, String section) throws Exception {
        if (section.equals("") || section.equals("section")) {
            link_contains_url(linkName, expectedUrl);
        } else {
//            String xpath = "(//*[@style = 'display: block;']//a[contains(text(), '" + linkName + "')])|(//a[contains(text(), '" + linkName + "')])";
            String xpath = ".//a[contains(text(), '" + linkName + "')]";

            WebElement element = currentPage.$(get_element(section)).then().findBy(xpath);
            highlight_element(xpath);
            if (expectedUrl.startsWith("#")) {
                expectedUrl = getDriver().getCurrentUrl() + expectedUrl;
            } else if (expectedUrl.startsWith("/")) {
                expectedUrl = DataProvider.ENVIROMENT_URL + expectedUrl;
            }
            show_message("expectedUrl = " + expectedUrl);

            Assert.assertTrue(expectedUrl + " url not found in " + linkName + "link. Currently url is " + element.getAttribute("href"), element.getAttribute("href").equals(expectedUrl));
        }
    }

    public void show_element_text(String xpath) {
        show_message(currentPage.$(xpath).getText());
    }


    @Step
    public void value_for_element(String singleValue) {

        //element - input
        //attribute - name
        String expectedParameter = singleValue.split("=")[0]; //lineType
        String expectedValue = singleValue.split("=")[1]; //Provide
        System.out.println("singleValue = " + singleValue);
        String value = currentPage.findBy("//*[@name='" + expectedParameter + "']").getAttribute("value");
        Assert.assertTrue(">element value is " + value + "<  should be " + singleValue, expectedValue.equalsIgnoreCase(value));
    }
//
//    public String current_route() {
//        String result = "";
//        String lineType = currentPage.findBy("//input[@name='lineType']").getAttribute("value");
//        String serviceOutcomeCode = currentPage.findBy("//input[@name='serviceOutcomeCode']").getAttribute("value");
//        String reasonCode = currentPage.findBy("//input[@name='reasonCode']").getAttribute("value");
//        String supplier = currentPage.findBy("//input[@name='supplier']").getAttribute("value");
//        System.out.println("@@@@@@@@@@@@@@@@@@@@################lineType " + lineType);
//        System.out.println("@@@@@@@@@@@@@@@@@@@@################serviceOutcomeCode " + serviceOutcomeCode);
//        System.out.println("@@@@@@@@@@@@@@@@@@@@################reasonCode " + reasonCode);
//        System.out.println("@@@@@@@@@@@@@@@@@@@@################supplier " + supplier);
//        String message = "linetype = " + lineType +
//                " serviceOutcomeCode = " + serviceOutcomeCode +
//                " reasonCode = " + reasonCode +
//                " supplier = " + supplier;
//        show_message(message);
//
//        if (serviceOutcomeCode.isEmpty()) {
//            if (lineType.equals("Provide")) {
//                result = "provide";
//            } else if (lineType.equals("TakeoverWorkingLine")) {
//                result = "takeoverworkingline";
//            } else if (lineType.equals("StartStoppedLine")) {
//                result = "startstoppedline";
//            } else if (lineType.equals("Import")) {
//                result = "import";
//            }
//        } else if (serviceOutcomeCode.equals("SAVC100")) {
//            result = "elp";
//        } else if (serviceOutcomeCode.equals("SAVC104")) {
//            if (reasonCode.equals("G")) {
//                result = "fibre regrade";
//            } else result = "adsl regrade";
//        }
//        System.out.println("@@@@@@@@@@@@@@@@@@@@#######result " + result);
//        if (!result.isEmpty()) {
//            if (supplier.isEmpty() || supplier.equals("BT")) {
//                result += " market 1";
//            }
//        }
//        System.out.println("#@#@#@#@#@ " + result);
//        return result;
//    }

    @Step
    public void page_content_correct(String pageName) throws Exception {

        File goalPage = new File(DataProvider.PAGES_TEMPLEATES_PATH + "/" + pageName);

        if (!goalPage.isDirectory()) {
            throw new Exception(pageName + " folder not found in " + DataProvider.PAGES_TEMPLEATES_PATH);
        }

        for (File section : goalPage.listFiles()) {
            check_section(section);
        }


    }

    @Step
    public void check_section(File section) throws Exception {
        String sectioName = section.getName();
        String expectedSectionText = new FileHelper(section).readFileAsString();
        see_message_in_section(expectedSectionText, sectioName);
    }

    ////////////////////////////////mobile
    //https://developer.android.com/reference/android/view/KeyEvent.html
    public int changeLetterToCode (char character) throws Exception {
        int c;
        switch (character) {
            case '0':
                c = 7;
                break;
            case '1':
                c = 8;
                break;
            case '2':
                c = 9;
                break;
            case '3':
                c = 10;
                break;
            case '4':
                c = 11;
                break;
            case '5':
                c = 12;
                break;
            case '6':
                c = 13;
                break;
            case '7':
                c = 14;
                break;
            case '8':
                c = 15;
                break;
            case '9':
                c = 16;
                break;
            case '*':
                c = 17;
                break;
            case '#':
                c = 18;
                break;
            case 'a':
                c = 29;
                break;
            case 'b':
                c = 30;
                break;
            case 'c':
                c = 31;
                break;
            case 'd':
                c = 32;
                break;
            case 'e':
                c = 33;
                break;
            case 'f':
                c = 34;
                break;
            case 'g':
                c = 35;
                break;
            case 'h':
                c = 36;
                break;
            case 'i':
                c = 37;
                break;
            case 'j':
                c = 38;
                break;
            case 'k':
                c = 39;
                break;
            case 'l':
                c = 40;
                break;
            case 'm':
                c = 41;
                break;
            case 'n':
                c = 42;
                break;
            case 'o':
                c = 43;
                break;
            case 'p':
                c = 44;
                break;
            case 'q':
                c = 45;
                break;
            case 'r':
                c = 46;
                break;
            case 's':
                c = 47;
                break;
            case 't':
                c = 48;
                break;
            case 'u':
                c = 49;
                break;
            case 'v':
                c = 50;
                break;
            case 'w':
                c = 51;
                break;
            case 'x':
                c = 52;
                break;
            case 'y':
                c = 53;
                break;
            case 'z':
                c = 54;
                break;
            case ',':
                c = 55;
                break;
            case '.':
                c = 56;
                break;
            case ' ':
                c = 62;
                break;
            case '-':
                c = 69;
                break;
            case '=':
                c = 70;
                break;
            case '[':
                c = 71;
                break;
            case ']':
                c = 72;
                break;
            case '\\':
                c = 73;
                break;
            case ';':
                c = 74;
                break;
            case '\'':
                c = 75;
                break;
            case '/':
                c = 76;
                break;
            case '@':
                c = 77;
                break;
            case '+':
                c = 81;
                break;
            default:
                throw new Exception(character + " is unsupported yet");
        }
        return c;
    }
}
