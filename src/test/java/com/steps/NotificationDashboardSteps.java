package com.steps;

import com.pages.LoginPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import org.openqa.selenium.WebElement;

/**
 * Created by dys on 08.09.2015.
 */
public class NotificationDashboardSteps extends BasicSteps {

    public NotificationDashboardSteps(Pages pages) {
        super(pages);
    }

    LoginPage loginPage;

    @Step
    public void open_login_page() throws Exception {
        getDriver().manage().deleteAllCookies();
        loginPage.open();
        getDriver().manage().window().maximize();
    }

    @Step
    public void click_on_element_in_section(String elementName, String section) throws Exception {
        String xpath = ".//div[contains(text(), \"" + elementName + "\")]";
        WebElement element = currentPage.$(get_element(section)).then().findBy(xpath);
        highlight_element(element.getAttribute("xpath"));
        element.click();
    }
}
