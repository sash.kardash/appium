package com.steps;

import com.pages.LoginPage;
import com.utils.Appium;
import io.appium.java_client.MobileElement;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static java.awt.event.KeyEvent.VK_A;

/**
 * Created by dys on 08.09.2015.
 */
public class LoginSteps extends BasicSteps {

    public LoginSteps(Pages pages) {
        super(pages);
    }

    LoginPage loginPage;
    Appium connector;
    String destDir;
    DateFormat dateFormat;


    @Step
    public void open_login_page() throws Exception {
        getDriver().manage().deleteAllCookies();
        loginPage.open();
        getDriver().manage().window().maximize();
    }

    @Step
    public void open_mobile_login_page() throws Exception {
        connector = new Appium();
    }

    public void login_with_mobile(String username, String password) throws Exception {


    }

    public void takeScreenShot() {
        // Set folder name to store screenshots.
        destDir = "C:/Users/a.kardash/Desktop/";
        // Capture screenshot.
        File scrFile = ((TakesScreenshot) connector.androidDriver).getScreenshotAs(OutputType.FILE);
        // Set date format to set It as screenshot file name.
        dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
        // Create folder under project with name "screenshots" provided to destDir.
        new File(destDir).mkdirs();
        // Set file name using current date time.
        String destFile = dateFormat.format(new Date()) + ".png";

        try {
            // Copy paste file at destination folder location
            FileUtils.copyFile(scrFile, new File(destDir + "/" + destFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void type(CharSequence characters) throws Exception {
        int length = characters.length();
        for (int i = 0; i < length; i++) {
            char character = characters.charAt(i);
            connector.androidDriver.sendKeyEvent(changeLetterToCode(character));
        }
    }


}
