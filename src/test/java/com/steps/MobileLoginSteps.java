package com.steps;

import com.pages.LoginPage;
import com.utils.Appium;
import com.utils.ImageUtil;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import org.apache.axis.security.servlet.ServletAuthenticatedUser;
import org.apache.commons.io.FileUtils;
import org.im4java.core.IM4JavaException;
import org.junit.Assert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by dys on 08.09.2015.
 */
public class MobileLoginSteps extends BasicSteps {

    public MobileLoginSteps(Pages pages) {
        super(pages);
    }

    LoginPage loginPage;
    Appium connector;
    String destDir;
    DateFormat dateFormat;


    @Step
    public void open_login_page() throws Exception {
        getDriver().manage().deleteAllCookies();
        loginPage.open();
        getDriver().manage().window().maximize();
    }

    @Step
    public void open_mobile_login_page() throws Exception {
        connector = new Appium();
        user_on_the_screen("LoginScreen");
    }

    public void login_with_mobile(String username, String password) throws Exception {
        type_in("asknhstest121101","username field");
        type_in("Ab1234!","password field");
        tap_on("sign in button");
    }

    @Step
    public boolean user_on_the_screen(String screenName) throws InterruptedException, IOException, IM4JavaException {
        ImageUtil imageUtil = new ImageUtil();
//        takeScreenShot(screenName+"1");
//        boolean imagePixels =  imageUtil.compare("screenshots/login.png","screenshots/login.png");
//        System.out.println("imagePixels = "+imagePixels);
//        boolean imagePixels =  imageUtil.compare("screenshots/sign in button.png","screenshots/terms1.png",false);
//        System.out.println("imagePixels = "+imagePixels);

//        boolean imagePixels =  imageUtil.compare("screenshots/"+screenName+"1.png","screenshots/"+screenName+".png");
//        System.out.println("imagePixels = "+imagePixels);
        waitABit(15000);
        DateFormat dateFormat = new SimpleDateFormat("HH-mm-ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43
        takeScreenShot("temp");
//        imageUtil.crop("screenshots/gold/"+screenName+".png","screenshots/temp.png",1200-30-30,700,30,960);
        System.out.println("screenshots/temp.png, screenshots/gold/"+screenName+".png, screenshots/"+screenName+"Difference.png");
        long nonWhitePixels = imageUtil.different_pixels("screenshots/gold/white.png","screenshots/temp.png","screenshots/nonWhitePixels.png");
        System.out.println("nonWhitePixels:"+nonWhitePixels);
        System.out.println("screenshots/temp.png, screenshots/gold/"+screenName+".png, screenshots/"+screenName+"Difference.png");
        long differentPixels = imageUtil.different_pixels("screenshots/temp.png","screenshots/gold/"+screenName+".png","screenshots/"+screenName+"Difference.png");

        System.out.println("differentPixels:"+differentPixels);
        Assert.assertTrue("Screenshot is different from "+screenName+" screen on "+(differentPixels/nonWhitePixels)*100+"%", differentPixels/nonWhitePixels <= 0.01);


//        imagePixels =  imageUtil.compare("screenshots/onsetb.png","screenshots/onsetb.png");
//        System.out.println("imagePixels = "+imagePixels);
//
//        boolean imagePixels =  imageUtil.compareImages("screenshots/login.png","screenshots/login1.png");
//        System.out.println("imagePixels = "+imagePixels);
//        imagePixels =  imageUtil.compareImages("screenshots/terms.png","screenshots/terms1.png", "screenshots/percentageDiff2.png");
//        System.out.println("imagePixels2 = "+imagePixels);

        return true;
    }

    @Step
    public void signup_with_mobile(String productcode) throws Exception {
        tap_on("sign up link");
        type_in(productcode, "product code field");
        tap_on("verify button");

//        type_in(productcode, "first name field");
//        type_in(productcode, "surname field");
//        type_in(productcode, "dob field");
//        type_in(productcode, "postcode field");
//        type_in(productcode, "email field");
//        type_in(productcode, "telephone field");
//        type_in(productcode, "username field");
//        type_in(productcode, "password field");
//        type_in(productcode, "confirm password field");
//        tap_on("sign up button");
        takeScreenShot();
    }

    @Step
    public void takeScreenShot() {
        // Set folder name to store screenshots.
        destDir = "screenshots";
        // Capture screenshot.
        File scrFile = ((TakesScreenshot) connector.androidDriver).getScreenshotAs(OutputType.FILE);
        // Set date format to set It as screenshot file name.
        dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
        // Create folder under project with name "screenshots" provided to destDir.
        new File(destDir).mkdirs();
        // Set file name using current date time.
        String destFile = dateFormat.format(new Date()) + ".png";

        try {
            // Copy paste file at destination folder location
            FileUtils.copyFile(scrFile, new File(destDir + "/" + destFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Step
    public void takeScreenShot(String name) {
        // Set folder name to store screenshots.
        destDir = "screenshots";
        // Capture screenshot.
        File scrFile = ((TakesScreenshot) connector.androidDriver).getScreenshotAs(OutputType.FILE);
        // Create folder under project with name "screenshots" provided to destDir.
        new File(destDir).mkdirs();
        // Set file name using current date time.
        String destFile = name + ".png";

        try {
            // Copy paste file at destination folder location
            FileUtils.copyFile(scrFile, new File(destDir + "/" + destFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Step
    public void type(CharSequence characters) throws Exception {
        int length = characters.length();
        for (int i = 0; i < length; i++) {
            char character = characters.charAt(i);

            String upperCaseCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            if (upperCaseCharacters.indexOf(character) >= 0) {
                connector.androidDriver.sendKeyEvent(changeLetterToCode(Character.toLowerCase(character)),1);
            } else if (character == '!'){
                connector.androidDriver.sendKeyEvent(8,1);
            } else connector.androidDriver.sendKeyEvent(changeLetterToCode(character));
        }
    }

    @Step
    public void type_in(String text, String fieldName) throws Exception {
        tap_on(fieldName);
        type(text);
        connector.androidDriver.hideKeyboard();
        waitABit(1000);
    }

    @Step
    public void swipe() throws Exception {
        connector.androidDriver.swipe(600, 1400, 600, 100, 800);
        waitABit(3000);
    }

    @Step
    public void tap_on(String elementName) throws Exception {
        int x = 600;
        int y = 1350;
        int t = 2000;
        boolean isSwipe = false;
        int xleft = 300;
        int xright = 900;
        int y1 = 1150;
        int y2 = 1300;
        int y3 = 1450;
        int y4 = 1600;
        int y5 = 800;
        int y6 = 950;
        int y7 = 1100;
        int y8 = 1250;
        int y9 = 1400;
        int y10 = 1600;

//        takeScreenShot(elementName+"1");

        switch (elementName) {
//general buttons
            case "agree button":
                x = 300;
                y = 1800;
                break;
            case "disagree button":
                x = 900;
                y = 1800;
                break;
            case "cancel button":
                x = xright;
                y = y1;
                break;
            case "ok button":
                x = xleft;
                y = y1;
                break;
            case "back button":
                x = 40;
                y = 40;
                t = 5000;
                break;
//Login screen
            case "sign up link":
                x = 850;
                y = 1750;
                break;
            case "product code field":
                y = 1150;
                break;
            case "verify button":
                y = 1450;
                break;
            case "first name field":
                y = 115;
                break;
            case "surname field":
                y = 300;
                break;
            case "dob field":
                y = 500;
                break;
            case "done button":
                y = 1300;
                break;
            case "postcode field":
                y = 850;
                break;
            case "email field":
                x = 600;
                y = 1050;
                break;
            case "telephone field":
                y = 1220;
                break;
            case "username field":
                y = 350;
                break;
            case "password field":
                y = 500;
                break;
            case "confirm password field":
                y = 1800;
                break;
            case "sign up button":
                y = 1900;
                break;
            case "sign in button":
                y = 800;
                break;
            case "sign in link":
                y = 1600;
                break;

//Symptom checker
            case "symptom checker item":
                y = y3;
                t = 60000;
                break;
            case "back head neck mouth item":
                y = y3;
                t = 8000;
                break;
            case "back item":
                y = y1;
                t = 7000;
                break;
            case "back pain item":
                y = y1;
                t = 22000;
                break;
            case "onset - 1-4 weeks ago item":
                isSwipe = true;
                y = y8;
                t = 21000;
                break;
            case "how are you feeling - same as usual item":
                y = y1;
                t = 21000;
                break;
            case "what caused your pain - not know item":
                y = y1;
                t = 22000;
                break;
            case "which part of your back hurting - i dont know item":
                y = y1;
                t = 25000;
                break;
            case "how bad is the pain now - no pain item":
                y = y1;
                t = 23000;
                break;
            case "does the pain spread towards another part - no item":
                y = y1;
                t = 20000;
                break;
            case "does anything make the pain worse - i donk know item":
                y = y1;
                t = 21000;
                break;
            case "do you feel dizzy when standing or sitting - no item":
                y = y1;
                t = 22000;
                break;
            case "have you had any diarrhoea - bowels are normal item":
                y = y1;
                t = 21000;
                break;
            case "are you passing urine normally - yes item":
                y = y1;
                t = 19000;
                break;
            case "have either your arms or legs weakened - my limb strength is normal item":
                y = y1;
                t = 13000;
                break;
            case "has touch sensation in legs or feet decreased - touch sensation normal item":
                y = y1;
                t = 13000;
                break;
            case "what was your last known temperature - i did not measure item":
                y = y1;
                t = 10000;
                break;
            case "do you have any other symptoms - None of these item":
                y = y1;
                t = 22000;
                break;
            case "when did your last menstrual period begin - 1-7 days ago item":
                y = y1;
                t = 22000;
                break;
            case "pregnancy - no item":
                y = y1;
                t = 19000;
                break;
            case "any other relevant illnesses - none of the ones listed item":
                y = y1;
                t = 23000;
                break;
            case "any history with back or neck problems - none of the ones listed item":
                y = y1;
                t = 23000;
                break;
            case "based on your responses it seems that you dont need immediate medical attention - i understand button":
                t = 28000;
                break;
            case "i recommend making an appointment - yes button":
                x = xleft;
                t = 27000;
                break;
            case "what would you like to do - transfer to gp button":
                x = xleft;
                t = 12000;
                break;
            case "confirm transfer - would you like me to place the call - cancel button":
                x = xright;
                y = y1;
                t = 3000;
                break;
            case "would you like information on self-care for back pain - yes button":
                x = xleft;
                t = 13000;
                break;
            case "i have information from nhs choices on back pain - back pain advice button":
                t = 7000;
                break;
            case "back pain back button":
                x = 40;
                y = 40;
                t = 2000;
                break;
            default:
                throw new Exception(elementName + " is unsupported yet");
        }

        if (isSwipe) {swipe();}
        connector.androidDriver.tap(1, x, y, 800);
        waitABit(t);
        waitABit(5000);
//        takeScreenShot(elementName);
//        user_on_the_screen(elementName);
    }


}
